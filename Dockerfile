# ==============================================================================
# Add https://gitlab.com/pipeline-components/org/base-entrypoint
# ------------------------------------------------------------------------------
FROM pipelinecomponents/base-entrypoint:0.4.0 as entrypoint

# ==============================================================================
# Component specific
# ------------------------------------------------------------------------------
FROM alpine:3.13.5
COPY app /app/
WORKDIR /app/
RUN echo "➡ Magic commands to make _template_ work go here ⬅"

# ==============================================================================
# Generic for all components
# ------------------------------------------------------------------------------
COPY --from=entrypoint /entrypoint.sh /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
ENV DEFAULTCMD _template_

WORKDIR /code/

# ==============================================================================
# Container meta information
# ------------------------------------------------------------------------------
ARG BUILD_DATE
ARG BUILD_REF

LABEL \
    maintainer="Robbert Müller <spam.me@grols.ch>" \
    org.label-schema.build-date=${BUILD_DATE} \
    org.label-schema.description="_Template_ in a container for gitlab-ci" \
    org.label-schema.name="_Template_" \
    org.label-schema.schema-version="1.0" \
    org.label-schema.url="https://pipeline-components.dev/" \
    org.label-schema.usage="https://gitlab.com/pipeline-components/_template_/blob/master/README.md" \
    org.label-schema.vcs-ref=${BUILD_REF} \
    org.label-schema.vcs-url="https://gitlab.com/pipeline-components/_template_/" \
    org.label-schema.vendor="Pipeline Components"
